
TARGETDIR=../public
PACKAGE_VERSION=$(node -p -e "require('../package.json').version")

rm -Rf "${TARGETDIR}"
mkdir -p "${TARGETDIR}"

cp -rf ./www/* "${TARGETDIR}/"
cp "../dist/ClipBoardMemo_Setup_${PACKAGE_VERSION}.exe" "${TARGETDIR}/download/win32/"
cp "../dist/ClipBoardMemo-${PACKAGE_VERSION}.dmg" "${TARGETDIR}/download/macos/"
cp "../dist/clipboardmemo_${PACKAGE_VERSION}_amd64.deb" "${TARGETDIR}/download/linux/"

cat ./www/index.html | sed "s/#PACKAGE_VERSION#/$PACKAGE_VERSION/g" >$TARGETDIR/index.html
