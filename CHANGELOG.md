# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.2]
### Add
- add german language support
- add database location selection
### Deprecated
- remove bootstrap deps from bower (switch to npm/yarn)

## [1.6.1]
### Fixed
- fix log errors 
### Changed
- change last entries keyboard shortcut from 0,1,2... to 1,2,..,0
### Deprecated
- Remove CI status from README

## [1.6.0]
- add keyboard shortcuts to copy the nth last clip in the clipboard

## [1.5.2]
- fix capture daemon error

## [1.5.1]
✔ (1.5.0) (1) fix bug "Some contents are poorly captured and causes a fatal corruption of the database" @done(19-01-05 18:53)

## [1.5.0]
✔ Log in file - https://www.npmjs.com/package/electron-log @done(18-12-21 16:43)
- fix not darwin os window position
- fix empty content capture hang
- Add images, HTML and clipboard type @done(18-12-16 10:50)
- Externalize clipboard, database and menu management 
- Add type field to database record @done(18-12-06 14:49)
- Externalize main process config @done(18-11-30 21:59)
- Secure cache file @done(18-11-30 21:58)

## [1.4.1] - 2018-11-23
- fix notifications
- add DB details window

## [1.3.6] - 2018-11-18
- create app website
- add CI GitLab jobs
- Change README to add languages support @done(18-11-11 11:36)
- Update LICENSE file @done(18-11-11 11:34)

## [1.3.5] - 2018-11-09
### Added
- Add languages selection (English, French, Spanish)
- Language by defaut set with locale
### Changed
- Fix DMG backgroung 

## [1.2.3] - 2018-11-08
### Changed
- fix clipboard cache in pause mode
- code revision

## [1.2.1] - 2018-11-02
### Changed
- First public release
### Deprecated
### Removed
### Fixed
### Security
