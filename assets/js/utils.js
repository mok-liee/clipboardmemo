
function getContentTypes (content) {
  return content.map(function (value, idx, arr) {
    return value.type
      .replace('text/plain', 'text')
      .replace('text/html', 'html')
      .replace('text/rtf', 'rtf')
      .replace(';base64', '')
      .replace('image/png', 'image')
      .replace('image/jpg', 'image')
  })
}

function getContentSize (record) {
  return record.reduce(function (prev, curr, curridx, arr) {
    return prev + curr.content.length
  }, 0)
}

module.exports = exports = {
  getContentTypes: getContentTypes,
  getContentSize: getContentSize
}
