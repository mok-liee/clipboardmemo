const electron = require('electron')
const globalShortcut = electron.globalShortcut
const Notification = electron.Notification
const log = require('electron-log')
const utils = require('./utils.js')

let MenuFactory = (function () {
  let self = {}

  self.init = function (config, prefs, i18n) {
    self.config = config
    self.prefs = prefs
    self.i18n = i18n
  }

  self.contentextract = function (content) {
    let lblsuite = ''
    if (content.length > self.config.get('app.dock.items.labelMaxChars')) {
      lblsuite = '...'
    }
    return content.trim().substr(0, self.config.get('app.dock.items.labelMaxChars')) + lblsuite
  }

  self.update = function (dbRecords, modePause, clickManager) {
    self.dbRecords = dbRecords
    self.modePause = modePause
    self.nbdisplayed = self.dbRecords.count()
    if (self.nbdisplayed > self.config.get('app.dock.items.displayMax')) {
      self.nbdisplayed = self.config.get('app.dock.items.displayMax')
    }
    let clickFunction = function (mnuitem, broWindow, event) {
      clickManager(mnuitem, broWindow, event)
    }
    let template = [
      {
        id: 'counts',
        label: self.nbdisplayed + ' / ' + self.dbRecords.count() + ' ' + self.i18n.t('ClipsRecorded'),
        type: 'normal',
        enabled: false
      },
      { type: 'separator' },
      {
        id: 'showDB',
        label: self.i18n.t('ShowDB'),
        type: 'normal',
        click: clickFunction,
        accelerator: self.config.get('app.shortcuts.historic')
      },
      {
        id: 'clear',
        label: self.i18n.t('ClearHistory'),
        type: 'normal',
        click: clickFunction
      },
      {
        id: 'pack',
        label: self.i18n.t('PackHistory'),
        type: 'normal',
        click: clickFunction
      },
      {
        id: 'pause',
        label: self.i18n.t('PauseCapture'),
        type: 'checkbox',
        checked: self.modePause,
        click: clickFunction
      },
      {
        id: 'pop',
        label: self.i18n.t('PopLastEntry'),
        type: 'normal',
        click: clickFunction,
        accelerator: self.config.get('app.shortcuts.pop')
      },
      { type: 'separator' },
      {
        id: 'about',
        label: self.i18n.t('About'),
        type: 'normal',
        click: clickFunction
      },
      { type: 'separator' },
      {
        type: 'submenu',
        label: self.i18n.t('Preferences'),
        submenu: [
          {
            id: 'startatlogin',
            label: self.i18n.t('StartAtLogin'),
            type: 'checkbox',
            checked: self.prefs.get('StartAtLogin', true),
            click: clickFunction
          },
          {
            id: 'notifications',
            label: self.i18n.t('Notifications'),
            type: 'checkbox',
            checked: self.prefs.get('Notifications', false),
            click: clickFunction
          },
          {
            id: 'databaseLocation',
            label: self.i18n.t('DatabaseLocation') + ': ' + self.prefs.get('databaseLocation'),
            type: 'normal',
            click: clickFunction
          },
          { type: 'separator' },
          {
            type: 'submenu',
            label: self.i18n.t('Language'),
            submenu: [{
              id: 'lng_de',
              label: self.i18n.t('LngGerman'),
              type: 'radio',
              checked: self.prefs.get('lng', self.config.get('i18n.fallbackLng')) === 'de',
              click: clickFunction
            },
            {
              id: 'lng_en',
              label: self.i18n.t('LngEnglish'),
              type: 'radio',
              checked: self.prefs.get('lng', self.config.get('i18n.fallbackLng')) === 'en',
              click: clickFunction
            },
            {
              id: 'lng_es',
              label: self.i18n.t('LngSpanish'),
              type: 'radio',
              checked: self.prefs.get('lng', self.config.get('i18n.fallbackLng')) === 'es',
              click: clickFunction
            },
            {
              id: 'lng_fr',
              label: self.i18n.t('LngFrench'),
              type: 'radio',
              checked: self.prefs.get('lng', self.config.get('i18n.fallbackLng')) === 'fr',
              click: clickFunction
            }
            ]
          }
        ]
      },
      { label: self.i18n.t('Quit'), type: 'normal', role: 'quit' }
    ]
    let cbMenuItems = []
    let shortItems = self.config.get('app.shortcuts.items')
    for (let i = 0; i < 10; i++) {
      globalShortcut.unregister(shortItems[i])
    }
    for (let i = 0; i < self.dbRecords.count(); i++) {
      if (i > self.config.get('app.dock.items.displayMax') - 1) break
      let record = self.dbRecords.get(i)
      if (record.type !== 'multi-value') continue
      let label = ''
      let contents = record.content
      let contentsTypes = utils.getContentTypes(contents)
      let textidx = contentsTypes.indexOf('text')
      if (textidx > -1) {
        label = self.contentextract(contents[textidx].content)
      } else {
        label = '-- (' + contentsTypes.toString() + ') --'
      }
      cbMenuItems.push({
        id: i,
        label: label,
        type: 'normal',
        accelerator: self.config.get('app.shortcuts.items')[i]
      })

      !(function outer(ii) {
        cbMenuItems[i].click = function() {
          clickFunction(cbMenuItems[i], null, null)
        }
        if (i < 10) {
          let ret = globalShortcut.register(shortItems[i], function() {
            clickManager(cbMenuItems[i], null, null)
          })
          if (!ret) {
            log.error('Error: registering shortcut ""' + shortItems[i] + '"" failed')
          }
        }
      })(i)
    }

    let fullMenuItems = cbMenuItems.concat(template)
    self.contextMenu = electron.Menu.buildFromTemplate(fullMenuItems)
    let oclear = self.contextMenu.getMenuItemById('clear')
    let opack = self.contextMenu.getMenuItemById('pack')
    let opop = self.contextMenu.getMenuItemById('pop')
    let showdb = self.contextMenu.getMenuItemById('showDB')
    if (self.dbRecords.count() === 0) {
      oclear.enabled = false
      opack.enabled = false
      opop.enabled = false
      showdb.enabled = false
    } else {
      oclear.enabled = true
      opack.enabled = true
      opop.enabled = true
      showdb.enabled = true
    }
    if (!Notification.isSupported()) {
      self.contextMenu.getMenuItemById('notifications').enabled = false
    }

    self.contextMenu.getMenuItemById('counts').label = '???'
    return self.contextMenu
  }

  return self
})()

// ----------------------------------------------------------------------------

let MenuFactorySingleton = (function () {
  let instance

  function createInstance () {
    let object = MenuFactory
    return object
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createInstance()
      }
      return instance
    }
  }
})()

// ----------------------------------------------------------------------------

module.exports = exports = MenuFactorySingleton
