# ClipboardMemo README

Copyright 2018,2019 - Fabrice Romand <fabrice.romand@gmail.com>  
ISC License (see LICENSE file)  
Icons credit :
- Sergey Ershov - https://www.iconfinder.com/iconsets/multimedia-75
- Bogdan Rosu - http://bogdanrosu.com/

App website : https://clipboardmemo.romand.ovh  

## Description

This application capture, store and help managing all clipboard new contents (raw/rtf/html texts and PNG/JPEG images).
It's a simple dock application for Linux, MacOS and Windows.

Each record can be
- replayed by a simple click on it
- moved to clipboard (Cmd+Alt+V). Extracted records are remove from history
- removed

Even more, the historic can be :
- totally cleaned
- packed into one single entry (text only)

Clipboard capture can be paused.

Currently, the following languages are supported :
- English
- French
- Spanish

## Keyboards shorcuts
- Alt+Cmd+0..9 (or Alt+Ctrl+0..9) to copy the nth last clip in the clipboard
- Alt+Cmd+H (or Alt+Ctrl+H) to display the DB details window
- Alt+Cmd+V (or Alt+Ctrl+V) to put the last entry of the stack in the clipboard. Then you can use Cmd+V (or Ctrl+V) to paste the content of the clipboard.

## Limits 
Currently limits applied :
- max. 80 records (rotating list)
- max. 16 last records are displayed

## Installing the dependencies

  \> yarn install

## Running application from the sources

  \> yarn run start

  or
  
   \> yarn run dev (for development mode)

## Building packages

Build for all platforms (Linux, MacOS,  Windows)

  \> yarn dist

Build only for one platform

  \> yarn dist:linux  
  \> yarn dist:darwin  
  \> yarn dist:win32

DMG packages will be in *./dist/* directory

## Issues reporting

https://gitlab.com/fabrom/clipboardmemo/issues

## Contributions

https://gitlab.com/fabrom/clipboardmemo/merge_requests
